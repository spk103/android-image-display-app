package com.example.androidphotos52;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class SlideShow extends AppCompatActivity {
    public ArrayList<String> list;
    public HashMap<String, BitmapSerializable> sourceMaps;
    ImageView slideShowPhoto;
    String pathToAlbums;
    String albumName;
    int index;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_show);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
//        list = bundle.getStringArrayList(Photos.listName);
//        sourceMaps = (HashMap<String, BitmapSerializable>) bundle.getSerializable(Photos.table);

        albumName = bundle.getString(MainActivity.albumName);
        pathToAlbums = bundle.getString(MainActivity.pathToAlbums);
        ObjectInputStream ois = null;


        try {
            ois = new ObjectInputStream(new FileInputStream(albumName));
            this.list = (ArrayList<String>) ois.readObject();
            this.sourceMaps = (HashMap<String,BitmapSerializable>) ois.readObject();
            ois.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }






        slideShowPhoto = findViewById(R.id.slideShowPhoto);
//        slideShowPhoto.setImageResource(R.drawable.white3);


        index = 0;

        if(list.size() > 0){
            slideShowPhoto.setImageBitmap(sourceMaps.get(list.get(index)).returnBitmap());
        }



    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Bundle bundle = new Bundle();
                bundle.putString(MainActivity.albumName,albumName);
                bundle.putString(MainActivity.pathToAlbums,pathToAlbums);
                Intent intent = new Intent(this, Photos.class);
                intent.putExtras(bundle);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void prev(View v){
        if(list.size() <= 0 || index >= list.size() || index < 0){
            return;
        }
        index = index - 1;
        if(index == -1){
            index = list.size()-1;
        }
        slideShowPhoto.setImageBitmap(sourceMaps.get(list.get(index)).returnBitmap());

    }

    public void next(View v){
        if(list.size() <= 0 || index >= list.size() || index < 0){
            return;
        }
        index = (index + 1) % list.size();
        slideShowPhoto.setImageBitmap(sourceMaps.get(list.get(index)).returnBitmap());
    }
}
package com.example.androidphotos52;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.loader.content.CursorLoader;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.CookieHandler;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

public class Photos extends AppCompatActivity {
    public String albumName;
    public String pathToAlbums;
    public static String listName = "list";
    public static String table = "table";
    ListView photosList;
    ListView nameList;
    ListView locationList;
    public ArrayAdapter<String> adapter;
    public ArrayAdapter<String> nameAdapter;
    public ArrayAdapter<String> locAdapter;
    public HashMap<String, ArrayList<String>> names;
    public  HashMap<String, ArrayList<String>> location;
    public ArrayList<String> image;
    public HashMap<String, BitmapSerializable> sourceMaps;
    public TextView nameTag;
    public TextView locationTag;
    public EditText tagPersonName;
    public EditText tagLocationName;
    public EditText moveToName;
    int index;
    int indexName;
    int indexLoc;
    int add=1;
    ImageView imageView;
    BroadCastShutOffRestart broadCastShutOffRestart;
    public Photos photo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle bundle = getIntent().getExtras();
        albumName = bundle.getString(MainActivity.albumName);
        pathToAlbums = bundle.getString(MainActivity.pathToAlbums);
        index = -1;
        indexName = -1;
        indexLoc = -1;

        this.photo = this;





        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(albumName));
            this.image = (ArrayList<String>) ois.readObject();
            this.sourceMaps = (HashMap<String,BitmapSerializable>) ois.readObject();
            this.names = (HashMap<String, ArrayList<String>>) ois.readObject();
            this.location = (HashMap<String, ArrayList<String>> ) ois.readObject();
            ois.close();
            System.out.println("All is well, all fields loaded");

        } catch (IOException | ClassNotFoundException e) {
            //e.printStackTrace();
            System.out.println("Something is null");
        }



 //       adapter = new ArrayAdapter<String>(this,R.layout.text,new ArrayList<>());
        adapter = new ArrayAdapter<String>(this,R.layout.text,image);
        nameAdapter = new ArrayAdapter<String>(this,R.layout.text,new ArrayList<>());
        locAdapter = new ArrayAdapter<String>(this,R.layout.text,new ArrayList<>());





        photosList = findViewById(R.id.photoList);
        photosList.setAdapter(adapter);
        LinearLayout part2 = (LinearLayout) getLayoutInflater().inflate(R.layout.content_photos_part2,null);
        photosList.addFooterView(part2);

        imageView = findViewById(R.id.photo);
        moveToName = findViewById(R.id.moveToName);
        nameList = findViewById(R.id.nameList);
        locationList = findViewById(R.id.locationList);

        nameList.setAdapter(nameAdapter);
        locationList.setAdapter(locAdapter);









        imageView.setImageResource(R.drawable.white3);


        photosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                index = position;
                String name = adapter.getItem(position);
                BitmapSerializable bitmapSerializable = sourceMaps.get(name);
                Bitmap bitmap = bitmapSerializable.returnBitmap();
                imageView.setImageBitmap(bitmap);
                nameAdapter.clear();
                locAdapter.clear();
                if(names.get(name) != null){
                    for(String tag : names.get(name)){
                        nameAdapter.add(tag);
                    }
                }

                if(location.get(name) != null && location.get(name).size() > 0){
                    for(String tag : location.get(name)){
                        locAdapter.add(tag);
                    }
                }


            }
        });

        nameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setSelected(true);
                indexName = i;
            }
        });

        locationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setSelected(true);
                indexLoc = i;
            }
        });

        nameTag = findViewById(R.id.name);
        locationTag = findViewById(R.id.location);
        tagPersonName = findViewById(R.id.tagPersonName);
        tagLocationName = findViewById(R.id.tagLocationName);

        broadCastShutOffRestart = new BroadCastShutOffRestart(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SHUTDOWN);
        filter.addAction(Intent.ACTION_REBOOT);
        registerReceiver(broadCastShutOffRestart,filter);




    }



    @Override
    protected void onStop() {
        try {
            save();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadCastShutOffRestart);
        super.onDestroy();
    }

    public void add(View v){
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,add );
        index = -1;
        indexName = -1;
        indexLoc = -1;

    }
    public void remove(View v){
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }

        String name = adapter.getItem(index);



        adapter.remove(name);
        sourceMaps.remove(name);
        names.remove(name);
        location.remove(name);

        nameTag.setText("");
        locationTag.setText("");
        imageView.setImageResource(R.drawable.white3);

        locAdapter.clear();
        nameAdapter.clear();

        index = -1;
        indexName = -1;
        indexLoc = -1;


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                unregisterReceiver(broadCastShutOffRestart);
                try {
                    save();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    public void tag_Person(View v){
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }
        String tag = tagPersonName.getText().toString();
        if(tag == null || tag.equals("")){
            return;
        }
        ArrayList<String> tags = names.get(adapter.getItem(index));

        for(String s : tags){
            if(s.equals(tag)){
                indexName = -1;
                return;
            }
        }


        names.get(adapter.getItem(index)).add(tag);
        nameAdapter.add(tag);
        //display();
        indexName = -1;
    }

    public void tag_Location(View v){
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }
        String tag = tagLocationName.getText().toString();
        if(tag == null || tag.equals("")){
            return;
        }

        ArrayList<String> tags = location.get(adapter.getItem(index));

        for(String s : tags){
            if(s.equals(tag)){
                indexName = -1;
                return;
            }
        }

        location.get(adapter.getItem(index)).add(tag);
        locAdapter.add(tag);
        //display();
        indexLoc = -1;
    }

    public void display (){
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }
        String name = adapter.getItem(index);
        BitmapSerializable bitmapSerializable = sourceMaps.get(name);
        Bitmap bitmap = bitmapSerializable.returnBitmap();
        imageView.setImageBitmap(bitmap);
        if(names.get(name) != null){
            for(String tag : names.get(name)){
                nameAdapter.add(tag);
            }
        }

        if(location.get(name) != null && location.get(name).size() > 0){
            for(String tag : location.get(name)){
                locAdapter.add(tag);
            }
        }
    }

    public void move (View v) throws IOException, ClassNotFoundException {
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }

        String albumToCopyTo = moveToName.getText().toString();
        int status = copy(albumToCopyTo);

        if(status == -1){
            index = -1;
            return;
        }

        String name = adapter.getItem(index);
        adapter.remove(name);
        sourceMaps.remove(name);
        names.remove(name);
        location.remove(name);

        nameTag.setText("");
        locationTag.setText("");
        imageView.setImageResource(R.drawable.white3);

        locAdapter.clear();
        nameAdapter.clear();

        index = -1;
        indexName = -1;
        indexLoc = -1;


    }

    public int copy (String toCopyTo) throws IOException, ClassNotFoundException {
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return -1;
        }

        String path = pathToAlbums + File.separator + toCopyTo;
        File file = new File (path);
        if(path.equals(albumName)){
            return -1;
        }

        if(!file.exists()){
            file = new File(toCopyTo);
            path = toCopyTo;
            if(!file.exists() || file.getAbsolutePath().equals(albumName)){
                return -1;
            }
        }

        String imageToCopy = adapter.getItem(index);

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
        ArrayList<String> copyImage = (ArrayList<String>) ois.readObject();
        HashMap<String,BitmapSerializable> copySourceMaps = (HashMap<String,BitmapSerializable>) ois.readObject();
        HashMap<String, ArrayList<String>> copyNames = (HashMap<String, ArrayList<String>>) ois.readObject();
        HashMap<String, ArrayList<String>> copyLocation = (HashMap<String, ArrayList<String>> ) ois.readObject();
        ois.close();

        if(!copyImage.contains(imageToCopy)){
            copyImage.add(imageToCopy);
        }

        copySourceMaps.put(imageToCopy,sourceMaps.get(imageToCopy));

        if(names.get(imageToCopy) != null){
            copyNames.put(imageToCopy, names.get(imageToCopy));
        }

        if(location.get(imageToCopy) != null){
            copyLocation.put(imageToCopy, location.get(imageToCopy));
        }

        file.delete();
        file.createNewFile();

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
        oos.writeObject(copyImage);
        oos.writeObject(copySourceMaps);
        oos.writeObject(copyNames);
        oos.writeObject(copyLocation);
        oos.close();

        return 0;






    }

    public void save() throws IOException, ClassNotFoundException {
        File file = new File(albumName);
        file.delete();
        file.createNewFile();
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(albumName));
        ArrayList<String> list = new ArrayList<>();

        for(int i = 0; i < adapter.getCount(); i++){
            list.add(adapter.getItem(i));
        }

        oos.writeObject(list);
        oos.writeObject(this.sourceMaps);
        oos.writeObject(this.names);
        oos.writeObject(this.location);
        oos.close();

        File directory = new File(pathToAlbums);
        File[] albums = directory.listFiles();

        for(File album : albums){
            if(album.isFile()){
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(album));
                ArrayList<String> readImages = (ArrayList<String>) ois.readObject();
                HashMap<String,BitmapSerializable> readSourceMaps = (HashMap<String,BitmapSerializable>) ois.readObject();
                HashMap<String, ArrayList<String>> readNames = (HashMap<String, ArrayList<String>>) ois.readObject();
                HashMap<String, ArrayList<String>> readLocation = (HashMap<String, ArrayList<String>> ) ois.readObject();
                ois.close();

                for(String image : list){
                    if(readImages.contains(image)){
                        readNames.remove(image);
                        readLocation.remove(image);

                        readNames.put(image,names.get(image));
                        readLocation.put(image,location.get(image));
                    }
                }

                oos = new ObjectOutputStream(new FileOutputStream(album));
                oos.writeObject(readImages);
                oos.writeObject(readSourceMaps);
                oos.writeObject(readNames);
                oos.writeObject(readLocation);
                oos.close();


            }
        }
    }

//    public void onBackPressed() {
//        super.onBackPressed();
//        System.out.println("yes");
//    }



    @Override
    protected void onActivityResult  (int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == add && data != null){
            Uri imageUri= data.getData();

            ImageDecoder.Source source = ImageDecoder.createSource(getApplicationContext().getContentResolver(), imageUri);
            Bitmap bitmap = null;


            try {
                bitmap = ImageDecoder.decodeBitmap(source);
            } catch (IOException e) {
                e.printStackTrace();
            }




            File file = new File(imageUri.getPath());
            String imageName = file.getName();



            for(int i = 0; i < this.adapter.getCount(); i++){
                if(imageName.equals(this.adapter.getItem(i))){
                    return;
                }
            }

            if(bitmap != null){
                imageView.setImageBitmap(bitmap);
            }
            nameTag.setText("");
            locationTag.setText("");

            this.adapter.add(imageName);
            sourceMaps.put(imageName, new BitmapSerializable(bitmap) );
            names.put(imageName, new ArrayList<String>());
            location.put(imageName,new ArrayList<String>());

            nameAdapter.clear();
            locAdapter.clear();

            File directory = new File(pathToAlbums);
            File[] albums = directory.listFiles();

            for(File album : albums){
                if(album.isFile() && !album.getAbsolutePath().equals(albumName)){
                    try{
                        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(album));
                        ArrayList<String> readImages = (ArrayList<String>) ois.readObject();
                        HashMap<String,BitmapSerializable> readSourceMaps = (HashMap<String,BitmapSerializable>) ois.readObject();
                        HashMap<String, ArrayList<String>> readNames = (HashMap<String, ArrayList<String>>) ois.readObject();
                        HashMap<String, ArrayList<String>> readLocation = (HashMap<String, ArrayList<String>> ) ois.readObject();
                        ois.close();


                            if(readImages.contains(imageName)){
                                names.put(imageName, readNames.get(imageName));
                                location.put(imageName, readLocation.get(imageName));
                                break;
                            }


                    }catch (Exception e){

                    }


                }
            }




            if(names.get(imageName) != null){
                for(String tag : names.get(imageName)){
                    nameAdapter.add(tag);
                }
            }

            if(location.get(imageName) != null && location.get(imageName).size() > 0){
                for(String tag : location.get(imageName)){
                    locAdapter.add(tag);
                }
            }
            //this.adapter.notifyDataSetChanged();
        }
    }


    public static String getPath(Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Images.Media._ID };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }


    public void slideShow(View view) throws IOException, ClassNotFoundException {
        save();
        Bundle bundle = new Bundle();
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < adapter.getCount(); i++){
            list.add(adapter.getItem(i));
        }

//        bundle.putStringArrayList(listName,list);
//        bundle.putSerializable(table,sourceMaps);
        bundle.putString(MainActivity.pathToAlbums,this.pathToAlbums);
        bundle.putString(MainActivity.albumName,this.albumName);
        Intent intent = new Intent(this, SlideShow.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    public void deletePersonTag(View view) {
        if(nameAdapter.getCount() <= 0 || indexName >= nameAdapter.getCount() || indexName < 0){
            return;
        }

        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }

        String tagName = nameAdapter.getItem(indexName);
        String name = adapter.getItem(index);

        names.get(name).remove(tagName);
        nameAdapter.remove(tagName);

        indexName= -1;

    }

    public void deleteLocationTag(View view) {
        if(locAdapter.getCount() <= 0 || indexLoc >= locAdapter.getCount() || indexLoc < 0){
            return;
        }

        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }

        String tagName = locAdapter.getItem(indexLoc);
        String name = adapter.getItem(index);

        location.get(name).remove(tagName);
        locAdapter.remove(tagName);

        indexLoc= -1;
    }
}
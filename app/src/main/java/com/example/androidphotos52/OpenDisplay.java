package com.example.androidphotos52;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class OpenDisplay extends AppCompatActivity {
    public ListView thumbList;
    public String albumName;
    public String pathToAlbums;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.activity_open_display);
        setContentView(R.layout.test);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thumbList = findViewById(R.id.thumbList);
        Bundle bundle = getIntent().getExtras();
        albumName = bundle.getString(MainActivity.albumName);
        pathToAlbums = bundle.getString(MainActivity.pathToAlbums);
//        ArrayAdapter<String>  adapter = new ArrayAdapter<String>(this,R.layout.text,new ArrayList<>());
//        for(int i = 0; i < 20; i++){
//            adapter.add(Integer.toString(i));
//        }
//        thumbList.setAdapter(adapter);



        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(albumName));
            ArrayList<String> image = (ArrayList<String>) ois.readObject();
            HashMap<String, BitmapSerializable> sourceMaps = (HashMap<String,BitmapSerializable>) ois.readObject();
            ois.close();
            if(image.size() > 0) {
                CustomListView customListView = new CustomListView(this, image, sourceMaps);
                thumbList.setAdapter(customListView);

            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


        //

    }

    public void ok(View v){

        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.albumName,albumName);
        bundle.putString(MainActivity.pathToAlbums,pathToAlbums);
        Intent intent = new Intent(this, Photos.class);
        intent.putExtras(bundle);
        startActivity(intent);


    }
}
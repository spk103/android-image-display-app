package com.example.androidphotos52;

import android.graphics.Bitmap;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class Search extends AppCompatActivity {
    ArrayList<String> list;
    HashMap<String,BitmapSerializable> sourceMaps;
    public HashMap<String, ArrayList<String>> names;
    public  HashMap<String, ArrayList<String>> location;
    ListView searchList;
    ListView searchNameList;
    ListView searchLocationList;
    ImageView searchPhoto;
    public ArrayAdapter<String> adapter;
    public ArrayAdapter<String> nameAdapter;
    public ArrayAdapter<String> locAdapter;
    public String albumsPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();

        ArrayList<String> argsPerson = bundle.getStringArrayList(MainActivity.argsListPerson);
        ArrayList<String> argsLocation = bundle.getStringArrayList(MainActivity.argsListLoc);
        int code = bundle.getInt(MainActivity.code);
        albumsPath = bundle.getString(MainActivity.pathToAlbums);
        list = new ArrayList<String>();
        sourceMaps = new HashMap<>();
        names = new HashMap<>();
        location = new HashMap<>();

        try {
            if(code == MainActivity.or){
                orPerson(argsPerson,list,sourceMaps,names,location);
                orLocation(argsLocation,list,sourceMaps,names,location);
            }
            else{
                and(argsPerson,argsLocation,list,sourceMaps,names,location);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        LinearLayout part2 = (LinearLayout) getLayoutInflater().inflate(R.layout.content_search_part2,null);

        adapter = new ArrayAdapter<String>(this,R.layout.text,list);
        nameAdapter = new ArrayAdapter<String>(this,R.layout.text,new ArrayList<>());
        locAdapter = new ArrayAdapter<String>(this,R.layout.text,new ArrayList<>());

        searchList = findViewById(R.id.searchList);

        searchList.setAdapter(adapter);
        searchList.addFooterView(part2);

        searchPhoto = findViewById(R.id.searchPhoto);
        searchPhoto.setImageResource(R.drawable.white3);
        searchNameList = findViewById(R.id.searchNameList);
        searchLocationList = findViewById(R.id.searchLocationList);

        searchNameList.setAdapter(nameAdapter);
        searchLocationList.setAdapter(locAdapter);


        searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(adapter.getCount() <= 0 || position >= adapter.getCount() || position < 0){
                    return;
                }


                view.setSelected(true);
                String name = adapter.getItem(position);
                BitmapSerializable bitmapSerializable = sourceMaps.get(name);
                Bitmap bitmap = bitmapSerializable.returnBitmap();
                searchPhoto.setImageBitmap(bitmap);
                nameAdapter.clear();
                locAdapter.clear();
                if(names.get(name) != null){
                    for(String tag : names.get(name)){
                        nameAdapter.add(tag);
                    }
                }

                if(location.get(name) != null && location.get(name).size() > 0){
                    for(String tag : location.get(name)){
                        locAdapter.add(tag);
                    }
                }


            }
        });





    }

    public void orPerson (ArrayList<String> args, ArrayList<String> list, HashMap<String,BitmapSerializable> sourceMaps, HashMap<String, ArrayList<String>> names, HashMap<String, ArrayList<String>> locations)throws ClassNotFoundException, IOException {



        for(String arg : args){
            tableFillerPerson(arg,list,sourceMaps,names,locations);
        }

    }

    public void orLocation (ArrayList<String> args, ArrayList<String> list, HashMap<String,BitmapSerializable> sourceMaps, HashMap<String, ArrayList<String>> names, HashMap<String, ArrayList<String>> locations)throws ClassNotFoundException, IOException {




        for(String arg : args){
            tableFillerLocation(arg,list,sourceMaps,names,locations);
        }

    }


    public void and (ArrayList<String> argsPerson, ArrayList<String> argsLocation, ArrayList<String> list, HashMap<String,BitmapSerializable> sourceMaps, HashMap<String, ArrayList<String>> names, HashMap<String, ArrayList<String>> locations)throws ClassNotFoundException, IOException {
        File directory = new File(albumsPath);
        File[] albums = directory.listFiles();

        for(File album : albums) {
            String albumPath = album.getAbsolutePath();
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(albumPath));
            ArrayList<String> imageRead = (ArrayList<String>) ois.readObject();
            HashMap<String, BitmapSerializable> sourceMapsRead = (HashMap<String, BitmapSerializable>) ois.readObject();
            HashMap<String, ArrayList<String>> namesRead = (HashMap<String, ArrayList<String>>) ois.readObject();
            HashMap<String, ArrayList<String>> locationRead = (HashMap<String, ArrayList<String>>) ois.readObject();
            ois.close();

            for(String image : imageRead){
                if(!list.contains(image)){
                    ArrayList<String> listNames = namesRead.get(image);
                    ArrayList<String> listLoc = locationRead.get(image);
                    if(containment(argsPerson,listNames) && containment(argsLocation,listLoc)){
                        list.add(image);
                        sourceMaps.put(image,sourceMapsRead.get(image));
                        names.put(image,namesRead.get(image));
                        locations.put(image,locationRead.get(image));
                    }
                }

            }


        }
    }

    public boolean containment (ArrayList<String> args, ArrayList<String> list){
        if(args.size() <= 0){
            return true;
        }


        for(String arg : args){
            boolean containment = false;
            for(String item : list){
               containment = containment || item.toLowerCase().contains(arg.toLowerCase());
            }
            if(!containment){
                return false;
            }
        }


        return true;
    }

    public void tableFillerPerson(String arg, ArrayList<String> list, HashMap<String,BitmapSerializable> sourceMaps, HashMap<String, ArrayList<String>> names, HashMap<String, ArrayList<String>> locations)throws ClassNotFoundException, IOException{
        File directory = new File(albumsPath);
        File[] albums = directory.listFiles();

        for(File album : albums){
            String albumPath = album.getAbsolutePath();
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(albumPath));
            ArrayList<String> imageRead = (ArrayList<String>) ois.readObject();
            HashMap<String,BitmapSerializable> sourceMapsRead = (HashMap<String,BitmapSerializable>) ois.readObject();
            HashMap<String, ArrayList<String>> namesRead = (HashMap<String, ArrayList<String>>) ois.readObject();
            HashMap<String, ArrayList<String>> locationRead = (HashMap<String, ArrayList<String>> ) ois.readObject();
            ois.close();

            for(String image : imageRead){
                if(!list.contains(image)){
                    if(namesRead.get(image) != null && !list.contains(image)){
                        for(String nameRead : namesRead.get(image)){
                            if(nameRead.toLowerCase().contains(arg.toLowerCase())){
                                list.add(image);
                                sourceMaps.put(image,sourceMapsRead.get(image));
                                names.put(image,namesRead.get(image));
                                locations.put(image, locationRead.get(image));
                            }
                        }
                    }
                }
            }
        }
    }

    public void tableFillerLocation(String arg, ArrayList<String> list, HashMap<String,BitmapSerializable> sourceMaps, HashMap<String, ArrayList<String>> names, HashMap<String, ArrayList<String>> locations)throws ClassNotFoundException, IOException{
        File directory = new File(albumsPath);
        File[] albums = directory.listFiles();

        for(File album : albums){
            String albumPath = album.getAbsolutePath();
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(albumPath));
            ArrayList<String> imageRead = (ArrayList<String>) ois.readObject();
            HashMap<String,BitmapSerializable> sourceMapsRead = (HashMap<String,BitmapSerializable>) ois.readObject();
            HashMap<String, ArrayList<String>> namesRead = (HashMap<String, ArrayList<String>>) ois.readObject();
            HashMap<String, ArrayList<String>> locationRead = (HashMap<String, ArrayList<String>> ) ois.readObject();
            ois.close();

            for(String image : imageRead){
                if(!list.contains(image)){
                    if(locationRead.get(image) != null && !list.contains(image)){
                        for(String locRead : locationRead.get(image)){
                            if(locRead.toLowerCase().contains(arg.toLowerCase())){
                                list.add(image);
                                sourceMaps.put(image,sourceMapsRead.get(image));
                                names.put(image,namesRead.get(image));
                                locations.put(image, locationRead.get(image));
                            }
                        }
                    }
                }

            }
        }
    }







}
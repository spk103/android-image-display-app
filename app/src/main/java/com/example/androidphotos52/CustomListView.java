package com.example.androidphotos52;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.util.*;

 //ArrayList <String> paths;

public class CustomListView extends ArrayAdapter {

    ArrayList<String> imagePaths;
    HashMap<String, BitmapSerializable> sourceMaps;
    private Activity context;

    public CustomListView(Activity context, ArrayList<String> imagePaths,  HashMap<String, BitmapSerializable> sourceMaps) {
        super(context, R.layout.thumbnail);
        //super(context, R.layout.test);
        this.context = context;
        this.imagePaths = imagePaths;
        this.sourceMaps = sourceMaps;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View r = convertView;
        ViewHolder viewHolder = null;
        if (r == null){
            LayoutInflater layoutInflater = context.getLayoutInflater();
            r = layoutInflater.inflate(R.layout.thumbnail,null,true);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);

        }
        else {
            viewHolder = (ViewHolder) r.getTag();
        }
        Bitmap bitmap = sourceMaps.get(imagePaths.get(position)).returnBitmap();
        viewHolder.imageView.setImageBitmap(bitmap);

        return r;
    }

    class ViewHolder {
        ImageView imageView;

        ViewHolder(View v){
            imageView = v.findViewById(R.id.thumbNail);
        }
    }
}

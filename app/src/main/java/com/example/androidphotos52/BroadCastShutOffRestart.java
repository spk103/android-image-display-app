package com.example.androidphotos52;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.io.IOException;

public class BroadCastShutOffRestart extends BroadcastReceiver {
    Photos photos;
    public BroadCastShutOffRestart(Photos photos){
        this.photos = photos;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_SHUTDOWN.equals(intent.getAction()) || Intent.ACTION_REBOOT.equals(intent.getAction())){
            try {
                photos.save();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}

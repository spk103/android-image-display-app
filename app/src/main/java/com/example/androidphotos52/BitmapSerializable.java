package com.example.androidphotos52;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class BitmapSerializable implements Serializable {
    public byte[] data;

    public BitmapSerializable (Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        data = byteArrayOutputStream.toByteArray();
    }

    public Bitmap returnBitmap() {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }


}

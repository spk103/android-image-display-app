package com.example.androidphotos52;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    public static String albumName = "albumName";
    public static String pathToAlbums = "pathToAlbums";
    public static String code = "code";
    public static int or = 1;
    public static int and = 0;
    public static String argsListPerson = "argsListPerson";
    public static String argsListLoc = "argsListLoc";

    public ListView albumsList;
    public String appPath;
    public String albumPath;
    public ArrayAdapter<String> adapter;
    public HashMap<String,String> fileMaps;
    public List<String> absPathAlbums;
    public EditText person;
    public EditText location;
    public EditText personTwo;
    public EditText locationTwo;


    public int index;
    public int STORAGE_PERMISSION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        albumsList = findViewById(R.id.albumsList);


        fileMaps = new HashMap<>();
        absPathAlbums = new ArrayList<>();
        index = 0;



        while(ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestStoragePermissions();
        }



        File album = new File(this.getFilesDir(), File.separator + "Albums");

        if(!album.exists()){
            album.mkdirs();
        }

        appPath = this.getFilesDir().getPath();
        albumPath = album.getAbsolutePath();

        adapter = new ArrayAdapter<String>(this,R.layout.text,new ArrayList<>());
        this.index = 0;

        File directory = new File(albumPath);
        File[] filesArray = directory.listFiles();

        for (File file : filesArray) {
            if (file.isFile()) {
                adapter.add(file.getName());
                absPathAlbums.add(file.getAbsolutePath());
                fileMaps.put(file.getName(), file.getAbsolutePath());
            }
        }


        albumsList.setAdapter(adapter);
        albumsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                index = position;
            }
        });

        LinearLayout part2 = (LinearLayout) getLayoutInflater().inflate(R.layout.album_part2,null);

        albumsList.addFooterView(part2);
        person = findViewById(R.id.person);
        location = findViewById(R.id.location);
        personTwo = findViewById(R.id.personTwo);
        locationTwo = findViewById(R.id.locationTwo);







    }

    public void open(View view) { //opens albums selected
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(albumName,fileMaps.get(adapter.getItem(index)));
        bundle.putString(pathToAlbums,albumPath);
        Intent intent = new Intent(this, OpenDisplay.class);
        intent.putExtras(bundle);
        startActivity(intent);


    }

    public void create(View view) throws IOException { //creates album with name user selected
        EditText create = findViewById(R.id.createAlbum);
        String name = create.getText().toString();
        if(name == null || name.equals("")){
            return;
        }
        for(int i = 0; i < adapter.getCount(); i++){
            if(name.equals(adapter.getItem(i))){
                return;
            }
        }

        String path = albumPath + File.separator + name;
        System.out.println(path);
        File file = new File(path);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("file not created");
            return;
        }

        path = file.getAbsolutePath();



        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
        oos.writeObject(new ArrayList<String>());
        oos.writeObject(new HashMap<String,BitmapSerializable>());
        oos.writeObject(new HashMap<String, ArrayList<String>>());
        oos.writeObject(new HashMap<String, ArrayList<String>>());
        oos.close();

        adapter.add(name);
        //albumsList.setAdapter(adapter);
        fileMaps.put(name,path);
        absPathAlbums.add(path);
        index = -1;



    }

    public void delete(View view) { //delete album selected
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }

        String name = adapter.getItem(index);
        String path = fileMaps.get(name);

        File file = new File (path);
        file.delete();

        adapter.remove(name);
        fileMaps.remove(name);
        absPathAlbums.remove(path);

       // albumsList.setAdapter(adapter);
        index = -1;





    }

    public void rename(View view) throws ClassNotFoundException, IOException { //renames album selected
        if(adapter.getCount() <= 0 || index >= adapter.getCount() || index < 0){
            return;
        }

        EditText rename = findViewById(R.id.renameAlbum);
        String newName = rename.getText().toString();

        String name = adapter.getItem(index);
        File oldFile = new File (fileMaps.get(name));


        File newFile = new File (albumPath +File.separator + newName );
        if(!newFile.exists()) {
            try {
                newFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            return;
        }


            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileMaps.get(name)));
            ArrayList<String> image = (ArrayList<String>) ois.readObject();
            HashMap<String, BitmapSerializable> sourceMaps = (HashMap<String,BitmapSerializable>) ois.readObject();
            HashMap<String, ArrayList<String>> names = (HashMap<String, ArrayList<String>>) ois.readObject();
            HashMap<String, ArrayList<String>> location = (HashMap<String, ArrayList<String>>) ois.readObject();
            ois.close();

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(newFile.getAbsolutePath()));
            oos.writeObject(image);
            oos.writeObject(sourceMaps);
            oos.writeObject(names);
            oos.writeObject(location);
            oos.close();

            oldFile.delete();

            adapter.remove(adapter.getItem(index));
            adapter.add(newName);

            absPathAlbums.remove(fileMaps.get(name));
            absPathAlbums.add(newFile.getAbsolutePath());

            fileMaps.remove(name);
            fileMaps.put(newName, newFile.getAbsolutePath());

            index = -1;





    }

    public void searchPerson(View view) throws ClassNotFoundException, IOException {
        String personName = person.getText().toString();
        if(personName == null || personName.equals("")){
            return;
        }

        ArrayList<String> argsPerson = new ArrayList<>();
        argsPerson.add(personName);

        ArrayList<String> argsLoc = new ArrayList<>();



        Bundle bundle = new Bundle();
        bundle.putStringArrayList(MainActivity.argsListPerson,argsPerson);
        bundle.putStringArrayList(MainActivity.argsListLoc,argsLoc);
        bundle.putInt(MainActivity.code,MainActivity.or);
        bundle.putString(MainActivity.pathToAlbums, albumPath);
        Intent intent = new Intent(this, Search.class);
        intent.putExtras(bundle);
        startActivity(intent);




    }

    public void person(ArrayList<String> list, HashMap<String,BitmapSerializable> sourceMaps, HashMap<String, ArrayList<String>> names, HashMap<String, ArrayList<String>> locations)throws ClassNotFoundException, IOException{

        String personName = person.getText().toString();
        personName = personName.toLowerCase();
        File directory = new File(albumPath);
        File[] albums = directory.listFiles();

        for(File album : albums){
            String albumPath = album.getAbsolutePath();
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(albumPath));
            ArrayList<String> imageRead = (ArrayList<String>) ois.readObject();
            HashMap<String,BitmapSerializable> sourceMapsRead = (HashMap<String,BitmapSerializable>) ois.readObject();
            HashMap<String, ArrayList<String>> namesRead = (HashMap<String, ArrayList<String>>) ois.readObject();
            HashMap<String, ArrayList<String>> locationRead = (HashMap<String, ArrayList<String>> ) ois.readObject();
            ois.close();

            for(String image : imageRead){
                if(namesRead.get(image) != null && !list.contains(image)){
                    for(String nameRead : namesRead.get(image)){
                        if(nameRead.toLowerCase().contains(personName)){
                            list.add(image);
                            sourceMaps.put(image,sourceMapsRead.get(image));
                            names.put(image,namesRead.get(image));
                            locations.put(image, locationRead.get(image));
                        }
                    }


                }
            }
        }

    }

    public void requestStoragePermissions(){
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},STORAGE_PERMISSION_CODE);
    }


    public void searchLocation(View view) {
        String locName = location.getText().toString();
        if(locName == null || locName.equals("")){
            return;
        }

        ArrayList<String> argsPerson = new ArrayList<>();


        ArrayList<String> argsLoc = new ArrayList<>();
        argsLoc.add(locName);



        Bundle bundle = new Bundle();
        bundle.putStringArrayList(MainActivity.argsListPerson,argsPerson);
        bundle.putStringArrayList(MainActivity.argsListLoc,argsLoc);
        bundle.putInt(MainActivity.code,MainActivity.or);
        bundle.putString(MainActivity.pathToAlbums, albumPath);
        Intent intent = new Intent(this, Search.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void or(View view) {
        String personName = person.getText().toString();
        String locName = location.getText().toString();
        String person2Name = personTwo.getText().toString();
        String location2Name = locationTwo.getText().toString();
        ArrayList<String> argsPerson = new ArrayList<>();
        ArrayList<String> argsLoc = new ArrayList<>();

        if(personName != null && !personName.equals("")){
            argsPerson.add(personName);
        }
        if(person2Name != null && !person2Name.equals("")){
            argsPerson.add(person2Name);
        }
        if(locName != null && !locName.equals("")){
            argsLoc.add(locName);
        }
        if(location2Name != null && !location2Name.equals("")){
            argsLoc.add(location2Name);
        }

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(MainActivity.argsListPerson,argsPerson);
        bundle.putStringArrayList(MainActivity.argsListLoc,argsLoc);
        bundle.putInt(MainActivity.code,MainActivity.or);
        bundle.putString(MainActivity.pathToAlbums, albumPath);
        Intent intent = new Intent(this, Search.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    public void and(View view) {
        String personName = person.getText().toString();
        String locName = location.getText().toString();
        String person2Name = personTwo.getText().toString();
        String location2Name = locationTwo.getText().toString();
        ArrayList<String> argsPerson = new ArrayList<>();
        ArrayList<String> argsLoc = new ArrayList<>();

        if(personName != null && !personName.equals("")){
            argsPerson.add(personName);
        }
        if(person2Name != null && !person2Name.equals("")){
            argsPerson.add(person2Name);
        }
        if(locName != null && !locName.equals("")){
            argsLoc.add(locName);
        }
        if(location2Name != null && !location2Name.equals("")){
            argsLoc.add(location2Name);
        }

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(MainActivity.argsListPerson,argsPerson);
        bundle.putStringArrayList(MainActivity.argsListLoc,argsLoc);
        bundle.putInt(MainActivity.code,MainActivity.and);
        bundle.putString(MainActivity.pathToAlbums, albumPath);
        Intent intent = new Intent(this, Search.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}